---
title: IAM agent
linkTitle: IAM agent
weight: 1
---

## What is this agent mean
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis convallis turpis orci, sed ultricies orci mollis imperdiet. Mauris eleifend tincidunt ante, nec congue nisi consectetur sit amet. Maecenas quis venenatis nisi, vel porta sem. Morbi et arcu hendrerit, placerat orci non, vestibulum nulla.
## Roles of IAM agent
### Authentication and validation
Praesent mauris turpis, viverra a velit quis, tempus placerat orci. Aliquam eleifend, est nec ultricies mollis, felis est facilisis mi, quis aliquam sem tellus non ipsum. Sed imperdiet tempor viverra. Pellentesque sodales libero mollis lobortis rhoncus. Ut sagittis gravida luctus. Nulla a elementum eros. Nunc sed ligula a magna semper semper ac eget sapien. 

Fusce molestie maximus turpis, et varius lectus euismod at. Donec eget fringilla nisi:  
- Phasellus auctor turpis diam, eu laoreet odio dapibus a.
- Nullam nec mauris vel augue lobortis ultricies sed vitae.

Phasellus lectus leo, fermentum eget tincidunt vel, ultricies eu magna. Quisque quis ligula sed justo maximus molestie in nec est. Vivamus euismod ornare fringilla. Quisque consectetur felis sed mauris tristique iaculis.

Aliquam nisl purus, lacinia a ornare sit amet, hendrerit in est. Curabitur maximus luctus elit, non auctor lacus lobortis non. Etiam convallis facilisis justo, et condimentum purus venenatis at.

Mauris semper ante eget augue consequat vulputate. Aenean facilisis dictum tortor, quis faucibus nulla porta at. Quisque eget tempus tortor. Nulla facilisi. Nam vel bibendum tellus.

*An example scenario depicting authentication and validation:*

1. Curabitur risus neque, blandit vel lectus eu.
2. Ut ut malesuada libero, sed sollicitudin quam. Nunc aliquam purus ex.
3. Nulla non velit odio.
4. Praesent suscipit ex ac elit cursus ornare. Nullam non nulla est.
5. Lorem ipsum dolor sit amet, consectetur:
    1. Sed dictum sodales arcu eu dictum.
    2. Quisque ac nunc eget sapien cursus efficitur vitae nec enim.
6. Donec rhoncus ex tellus, eget congue diam condimentum vel.
7. Aliquam aliquet, ante quis consectetur sagittis, ligula eros ultricies quam, non commodo sapien nisi quis urna.
8. Nam eget ornare massa, blandit interdum elit. Vivamus id consequat neque, at lacinia odio.
### Space switching: Passing dataspace ID to SAS filter
Proin diam ipsum, sagittis eget quam suscipit, pellentesque accumsan augue. Maecenas molestie id urna eget vestibulum. Vivamus commodo et diam at malesuada. Donec gravida, lectus vitae volutpat maximus, massa odio tincidunt ex.

sed dictum augue ligula vel ligula. In justo erat, tincidunt ac tristique eget, pretium id risus. Praesent eleifend ac mi at vulputate. Donec rhoncus ex tellus, eget congue diam condimentum vel.
### Reducing server calls 
Pellentesque nibh augue, dignissim id erat porttitor, ultricies congue eros. Morbi id eros fermentum, interdum erat in, auctor ipsum. Fusce fermentum consectetur lacus, at tincidunt lacus pellentesque quis. Integer dapibus nisl quis velit condimentum blandit. Sed vehicula, justo non dictum convallis, tellus felis imperdiet enim, nec fermentum enim ex egestas diam.
### Clearing sessions
Aliquam vestibulum magna nec mi mollis, ut gravida est rhoncus. Nulla sollicitudin nibh ac cursus convallis. Phasellus blandit et metus ornare interdum. Morbi scelerisque lobortis metus quis vehicula. Donec at nulla turpis. Fusce bibendum non nunc eu mattis. Vestibulum volutpat turpis at metus rhoncus eleifend facilisis non ante.
## How to set up IAM agent with your app
### Step 1: Bundle IAM agent with your app
   
> Note: Cras imperdiet neque et dolor eleifend, id dapibus ipsum lacinia. Refer to [Proin aliquet arcu a aliquet gravida](www.google.com).


1. Maecenas purus erat, condimentum ac at [https://zylker.com/somefilestored/in_here](www.google.com).
2. Fusce sodales feugiat **Sed commodo.zip** file.
3. Fusce sodales feugiat **ytr** folder.
4. Fusce sodales feugiat **oig** folder.
5. Fusce sodales "lobortis.txt" and "feugiat_venenatis.txt" from the **bugh** folder.

*Reference*: [Ant task](www.google.com)

Cras luctus gravida velit, sit amet sodales augue placerat tincidunt.
### Step 2: Configure security.xml file
Donec condimentum iaculis quam non accumsan:
1. Vestibulum porttitor, elit non dignissim dictum, tellus arcu tempor:
    - `<vitae aliquam="blandit.jhdd" value="{your posuere purus quis}" />`
    - `<vitae aliquam="ac.ametqu.non.libero.semper" value="{your ornare odio porttitor" />`
    - `<vitae aliquam="ac.non" value="true" />`
    - `<vitae aliquam="ac.non.sapien" value="true" />` (optional: Suspendisse ut lectus nec augue venenatis)

2. Aliquam et efficitur nisi. Integer nisi dui, viverra a molestie eu.
    - Donec vulputate eleifend rhoncus, add `<vitae aliquam="ac.ametqu.non.libero.semper" value="{lobortis_name}.{bibendum_extended_classname}" />`
    - Aliquam vitae aliquam dolor, add `<vitae aliquam="ac.ametqu.non.libero.semper" value="{lobortis_name}.{bibendum_extended_classname}" />`
    - Fusce facilisis metus, add `vitae aliquam="ac.ametqu.non.libero.semper="{package_name}.{lobortis_extended_classname}" />`

3. Donec euismod est eu odio mattis, a iaculis ipsum venenatis.

> Note: Nam fringilla laoreet velit, vitae ornare ligula gravida nec. Refer to [Donec condimentum iaculis](www.google.com).

### Step 3: Use Aliquam class to call UIY APIs
Proin lacinia facilisis vehicula. Etiam ac ex libero. Aenean quam felis, molestie et tincidunt nec, malesuada quis quam.

![IAMProxy class](/images/proxy_class.png)

*Reference*: [Example involving scelerisque sit amet quam](www.google.com)

# This a extra heading